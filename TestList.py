import functools
import operator
import sqlite3
from typing import List, Any

from PyQt5.QtWidgets import QListWidgetItem

from Test import Test
from Settings import Settings


class TestList:

    def __init__(self, name, tests=None, settings=None):
        self.name = name
        try:
            if tests is None:
                pass
                 #self.tests = self.get_tests()

            elif isinstance(tests[0], Test):
                self.tests = []
                for i in tests:
                    self.tests.append(i)
            elif isinstance(tests[0], QListWidgetItem):
                print ("type {0} Value {1}".format(type(tests[0]), tests[0].text()))

                self.tests = []
                for i in tests:
                    test=Test(i.text())
                    print("name: {0} command: {1} ID: {2}".format(test.name, test.command, test.id))
                    self.tests.append(test)
        except IndexError:pass

        if isinstance(settings, Settings) or settings is None:
            self.settings=settings

    # WIP
    def execute(self, output_qlist = None):
        if output_qlist is None:
            print("executing tests")
            for i in self.tests:
                i.execute()
        else:
            for i in self.tests:
                output_qlist.addItem("Executing: {0}".format(i.name))
                output_qlist.addItem(i.execute())


    def insert(self):
        id = self.insert_list_name()
        self.insert_list(id)

    def insert_list_name(self):
        conn = sqlite3.connect('db/AnyfeederBosch.sqlite')
        cursor = conn.cursor()
        cursor.execute('INSERT INTO TestList(name) VALUES (?)', (self.name,))
        conn.commit()
        output = cursor.lastrowid
        cursor.close()
        conn.close()
        return output

    def insert_list(self, test_list_id):
        conn = sqlite3.connect('db/AnyfeederBosch.sqlite')
        cursor = conn.cursor()
        for i in self.tests:
            arg = (test_list_id, i.id)
            cursor.execute('INSERT INTO TestList_Test(TestList_ID, Test_ID) Values (?,?)', arg)
        conn.commit()
        output = cursor.lastrowid
        cursor.close()
        conn.close()
        return output
