import sqlite3


class Settings:
    def __init__(self,
                 id=None,
                 ffwd_turns=None,
                 ffwd_speed=None,
                 fbwd_turns=None,
                 fbwd_speed=None,
                 flipfwd_turns=None,
                 flipfwd_speed=None,
                 flipbwd_turns=None,
                 flipbwd_speed=None,
                 flip_turns=None,
                 flip_speed=None,
                 disp_turns=None,
                 disp_speed=None,
                 purge_turns=None,
                 purge_speed=None,
                 ):
        self.id = id
        self.ffwd_turns = ffwd_turns
        self.ffwd_speed = ffwd_speed
        self.fbwd_turns = fbwd_turns
        self.fbwd_speed = fbwd_speed
        self.flipfwd_turns = flipfwd_turns
        self.flipfwd_speed = flipfwd_speed
        self.flipbwd_turns = flipbwd_turns
        self.flipbwd_speed = flipbwd_speed
        self.flip_turns = flip_turns
        self.flip_speed = flip_speed
        self.disp_turns = disp_turns
        self.disp_speed = disp_speed
        self.purge_turns = purge_turns
        self.purge_speed = purge_speed

    def __str__(self):
        pass

    def insert(self):
        sql = """INSERT INTO Settings (
                         ffwd_turns,
                         ffwd_speed,
                         fbwd_turns,
                         fbwd_speed,
                         flipfwd_turns,
                         flipfwd_speed,
                         flipbwd_turns,
                         flipbwd_speed,
                         flip_turns,
                         flip_speed,
                         disp_turns,
                         disp_speed,
                         purge_turns,
                         purge_speed
                     )
                     VALUES (
                         ?,?,?,?,?,?,?,?,?,?,?,?,?,?
                     );"""
        conn = sqlite3.connect('db/AnyfeederBosch.sqlite')
        cursor = conn.cursor()
        cursor.execute(sql, (
            self.ffwd_turns,
            self.ffwd_speed,
            self.fbwd_turns,
            self.fbwd_speed,
            self.flipfwd_turns,
            self.flipfwd_speed,
            self.flipbwd_turns,
            self.flipbwd_speed,
            self.flip_turns,
            self.flip_speed,
            self.disp_turns,
            self.disp_speed,
            self.purge_turns,
            self.purge_speed
        ))
        conn.commit()
        output = cursor.lastrowid
        cursor.close()
        conn.close()
        self.id = output
        return self
