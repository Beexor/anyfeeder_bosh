import functools
import operator

from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtCore import QObject, pyqtSignal

from Test import Test
from TestList import TestList
from ui.MainMenuWindow import MainMenuWindow
from db.DatabaseManager import DatabaseManager
import sys


def main():
    app = QtWidgets.QApplication(sys.argv)
    application = MainMenuWindow()

    # TODO DELETE BEFORE PRODUCTION
    DatabaseManager.recreate_database()
    tests = []
    for i in range(0, 5):
        tests.append(Test("Initialize"))
        tests.append(Test("Flip"))
        tests.append(Test("Initialize"))
        tests.append(Test("Flip"))
    print(' '.join(map(str, tests)))
    testList = TestList("some list", tests)
    testList.insert()

    application.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
