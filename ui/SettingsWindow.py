import sqlite3
from PyQt5 import QtWidgets, uic

from Settings import Settings
from Test import Test
from TestList import TestList


class SettingsWindow(QtWidgets.QMainWindow):

    def __init__(self, ListBuilder_window):

        super(SettingsWindow, self).__init__()
        uic.loadUi('ui/SettingsWindow.ui', self)
        self.show()

        self.ListBuilder_window = ListBuilder_window
        self.btn_save.clicked.connect(self.save)
        self.btn_back.clicked.connect(self.back)

    def back(self):
        self.ListBuilder_window.show()
        self.close()

    def save(self):
        settings = Settings()

        settings.ffwd_turns = self.txt_ffwd_turns.text()
        settings.ffwd_speed = self.txt_ffwd_speed.text()
        settings.fbwd_turns = self.txt_fbwd_turns.text()
        settings.fbwd_speed = self.txt_fbwd_speed.text()
        settings.flipfwd_turns = self.txt_flipfwd_turns.text()
        settings.flipfwd_speed = self.txt_flipfwd_speed.text()
        settings.flipbwd_turns = self.txt_flipbwd_turns.text()
        settings.flipbwd_speed = self.txt_flipbwd_speed.text()
        settings.flip_turns = self.txt_flip_turns.text()
        settings.flip_speed = self.txt_flip_speed.text()
        settings.disp_turns = self.txt_disp_turns.text()
        settings.disp_speed = self.txt_disp_speed.text()
        settings.purge_turns = self.txt_purge_turns.text()
        settings.purge_speed = self.txt_purge_speed.text()
        self.ListBuilder_window.settings = settings
        return settings.insert()

