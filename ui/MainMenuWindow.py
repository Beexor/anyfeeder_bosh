from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtCore import QObject, pyqtSignal
from PyQt5.QtWidgets import QMessageBox

from ui.ListBuilderWindow import ListBuilderWindow
from ui.ListExecutor import ListExecutor
from ui.ManualTestWindow import ManualTestWindow
from db.DatabaseManager import DatabaseManager
import sys

from ui.SettingsWindow import SettingsWindow


class MainMenuWindow(QtWidgets.QDialog):
    add_trigger = pyqtSignal()

    def __init__(self):
        super(MainMenuWindow, self).__init__()
        uic.loadUi('ui/MainMenuWindow.ui', self)
        self.show()

        self.btn_manual.clicked.connect(self.open_manual_test_indow)
        self.btn_create.clicked.connect(self.open_list_builder_window)
        self.btn_run.clicked.connect(self.open_list_executor)
        self.btn_reset.clicked.connect(self.reset_database)

    def open_manual_test_indow(self):
        self.window = ManualTestWindow(self)
        self.window.show()
        self.close()

    def open_list_builder_window(self):
        self.window = ListBuilderWindow(self)
        self.window.show()
        self.close()

    def open_list_executor(self):
        self.window = ListExecutor(self)
        self.window.show()
        self.close()

    def reset_database(self):
        DatabaseManager.recreate_database()
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("The database has been reset")
        msg.setWindowTitle("Alert")
        msg.exec_()



