import sqlite3
from PyQt5 import QtWidgets, uic
from Test import Test
from TestList import TestList


class ListExecutor(QtWidgets.QMainWindow):

    def __init__(self, main_window):
        super(ListExecutor, self).__init__()
        uic.loadUi('ui/ListExecutor.ui', self)
        self.show()

        self.main_window = main_window
        self.btn_run.clicked.connect(self.execute_tests)
        self.btn_back.clicked.connect(self.back)

        self.load_list()

    def execute_tests(self):
        item = self.lst_tests.currentItem()
        lst_args = item.text().split('. ')
        list_id = lst_args[0]
        name = lst_args[1]
        print(list_id)
        out = self.select_test_list(list_id)
        tests = []
        for i in out:
            test = Test(id=i[0], name=i[1], command=i[2])
            print(test)
            tests.append(test)

        test_list = TestList(name, tests)

        test_list.execute(self.lst_output)


    def back(self):
        self.main_window.show()
        self.close()

    def load_list(self):
        tup_names = self.select_all_names()
        lst_names = []
        for i in tup_names:
            lst_names.append("{0}. {1}".format(i[0], i[1]))
        self.lst_tests.addItems(lst_names)

    def select_all_names(self):
        conn = sqlite3.connect('db/AnyfeederBosch.sqlite')
        cursor = conn.cursor()
        sql = 'SELECT * FROM TestList'
        cursor.execute(sql)
        output = cursor.fetchall()
        cursor.close()
        conn.close()
        print("output = ")
        print(output)
        return output

    def select_test_list(self, id):
        conn = sqlite3.connect('db/AnyfeederBosch.sqlite')
        cursor = conn.cursor()
        sql = '''SELECT
                    Tests.ID,
                    Tests.Name,
                    Tests.Command
                FROM
                    TestList_Test TestList_Test,
                    Tests Tests,
                    TestList TestList
                WHERE
                    TestList_Test.TestList_ID = TestList.ID AND
                    TestList_Test.Test_ID = Tests.ID AND
                    TestList.ID = ?'''
        cursor.execute(sql, id)
        out = list(cursor.fetchall())
        print(' '.join(map(str, out)))
        cursor.close()
        conn.close()
        return out
