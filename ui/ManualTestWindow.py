from PyQt5 import QtWidgets, uic
from Test import Test


class ManualTestWindow(QtWidgets.QMainWindow):

    def __init__(self, main_window):
        super(ManualTestWindow, self).__init__()
        uic.loadUi('ui/ManualTestWindow.ui', self)

        self.main_window = main_window
        # zegt iet als ge dees ziet in ne code review
        self.btn_init.clicked.connect(self.init)
        self.btn_ffwd.clicked.connect(self.ffwd)
        self.btn_fbwd.clicked.connect(self.fbwd)
        self.btn_flipfwd.clicked.connect(self.flipfwd)
        self.btn_flipbwd.clicked.connect(self.flipbwd)
        self.btn_flip.clicked.connect(self.flip)
        self.btn_disp.clicked.connect(self.disp)
        self.btn_purge.clicked.connect(self.purge)
        self.btn_stop.clicked.connect(self.stop)
        self.btn_rsterror.clicked.connect(self.rsterror)
        self.btn_back.clicked.connect(self.back)

    def flip_button(self, state):
        self.btn_init.setEnabled(state)
        if state:
            self.btn_init.clicked.connect(self.init)
        else:
            self.btn_init.clicked.disconnect(self.init)

    def run_test(self, name):
        self.flip_button(False)
        self.lst_test.addItem(name)
        test = Test(name)
        self.lst_test.addItem(test.execute())
        self.repaint()
        self.flip_button(True)
        self.repaint()

    def init(self):
        self.run_test("Initialize")

    def ffwd(self):
        self.run_test("Feed forward")

    def fbwd(self):
        self.run_test("Feed backwards")

    def flipfwd(self):
        self.run_test("Feed/flip forward")

    def flipbwd(self):
        self.run_test("Feed/flip backwards")

    def flip(self):
        self.run_test("Flip")

    def disp(self):
        self.run_test("Dispense")

    def purge(self):
        self.run_test("Purge")

    def stop(self):
        self.run_test("Stop")

    def rsterror(self):
        self.run_test("Reset error")

    def back(self):
        self.main_window.show()
        self.close()
