from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtCore import QObject, pyqtSignal
from PyQt5.QtWidgets import QMessageBox

from TestList import TestList
from ui.SettingsWindow import SettingsWindow


class ListBuilderWindow(QtWidgets.QMainWindow):
    add_trigger = pyqtSignal()

    def __init__(self,main_window):
        self.testlist=TestList(None)
        super(ListBuilderWindow, self).__init__()
        uic.loadUi('ui/ListBuilderWindow.ui', self)
        self.main_window = main_window

        self.btn_init.clicked.connect(self.init)
        self.btn_ffwd.clicked.connect(self.ffwd)
        self.btn_fbwd.clicked.connect(self.fbwd)
        self.btn_flipfwd.clicked.connect(self.flipfwd)
        self.btn_flipbwd.clicked.connect(self.flipbwd)
        self.btn_flip.clicked.connect(self.flip)
        self.btn_disp.clicked.connect(self.disp)
        self.btn_purge.clicked.connect(self.purge)
        self.btn_stop.clicked.connect(self.stop)
        self.btn_rsterror.clicked.connect(self.rsterror)
        self.btn_back.clicked.connect(self.back)
        self.btn_save.clicked.connect(self.save)
        self.btn_settings.clicked.connect(self.open_settings)

    def init(self):
        self.lst_test.addItem('Initialize')
        print("init")

    def ffwd(self):
        self.lst_test.addItem('Feed forward')
        print("ffwd")

    def fbwd(self):
        self.lst_test.addItem('Feed backwards')
        print("fbwd")

    def flipfwd(self):
        self.lst_test.addItem('Feed/flip forward')
        print("flipfwd")

    def flipbwd(self):
        self.lst_test.addItem('Feed/flip backwards')
        print("flipbwd")

    def flip(self):
        self.lst_test.addItem('Flip')
        print("flip")

    def disp(self):
        self.lst_test.addItem('Dispense')
        print("disp")

    def purge(self):
        self.lst_test.addItem('Purge')
        print("purge")

    def stop(self):
        self.lst_test.addItem('Stop')
        print("stop")

    def rsterror(self):
        self.lst_test.addItem('Reset error')
        print("rsterror")

    def save(self):
        items = []
        for i in range(self.lst_test.count()):
            items.append(self.lst_test.item(i))
        self.testlist.__init__(self.txt_name.text(), items)
        self.testlist.insert()
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("{0} has been saved".format(self.txt_name.text()))
        msg.setWindowTitle("Alert")
        msg.exec_()
        print("save")

    def delete_selected(self):

        items = self.lst_test.selectedItems()
        for i in items:
            self.lst_test.takeItem(self.lst_test.row(i))
        print("deleted")

    def back(self):
        self.main_window.show()
        self.close()

    def open_settings(self):
        items = []
        for i in range(self.lst_test.count()):
            items.append(self.lst_test.item(i))
        testlist = TestList(self.txt_name.text(), items)
        self.window = SettingsWindow(self)
        self.window.show()
        self.close()