--
-- File generated with SQLiteStudio v3.2.1 on Wed Jun 10 14:38:11 2020
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Settings
DROP TABLE IF EXISTS Settings;

CREATE TABLE Settings (
    ID            INTEGER PRIMARY KEY AUTOINCREMENT
                          UNIQUE
                          NOT NULL,
    ffwd_turns    INTEGER,
    ffwd_speed    INTEGER,
    fbwd_turns    INTEGER,
    fbwd_speed    INTEGER,
    flipfwd_turns INTEGER,
    flipfwd_speed INTEGER,
    flipbwd_turns INTEGER,
    flipbwd_speed INTEGER,
    flip_turns    INTEGER,
    flip_speed    INTEGER,
    disp_turns    INTEGER,
    disp_speed    INTEGER,
    purge_turns   INTEGER,
    purge_speed   INTEGER
);


-- Table: TestList
DROP TABLE IF EXISTS TestList;

CREATE TABLE TestList (
    ID          INTEGER PRIMARY KEY ASC AUTOINCREMENT
                        UNIQUE
                        NOT NULL,
    Name        STRING,
    Settings_ID INTEGER REFERENCES Settings (ID) 
);


-- Table: TestList_Test
DROP TABLE IF EXISTS TestList_Test;

CREATE TABLE TestList_Test (
    ID          INTEGER PRIMARY KEY AUTOINCREMENT
                        UNIQUE
                        NOT NULL,
    TestList_ID INTEGER REFERENCES TestList (ID) 
                        NOT NULL,
    Test_ID     INTEGER REFERENCES Tests (ID) 
                        NOT NULL
);


-- Table: Tests
DROP TABLE IF EXISTS Tests;

CREATE TABLE Tests (
    ID      INTEGER PRIMARY KEY ASC AUTOINCREMENT
                    UNIQUE
                    NOT NULL,
    Name    STRING  UNIQUE
                    NOT NULL,
    Command STRING  UNIQUE
                    NOT NULL
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
