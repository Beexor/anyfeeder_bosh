import sqlite3


class DatabaseManager:

    @staticmethod
    def recreate_database():
        tests = [
            ("Initialize", "x=16"),
            ("Feed forward", "x=1"),
            ("Feed backwards", "x=2"),
            ("Feed/flip forward", "x=3"),
            ("Feed/flip backwards", "x=4"),
            ("Flip", "x=5"),
            ("Dispense", "x=6"),
            ("Purge", "x=7"),
            ("Stop", "x=15"),
            ("Reset error", "x=30"),
        ]
        create_query = open('db/ExportedDB.sql', 'r').read()
        conn = sqlite3.connect('db/AnyfeederBosch.sqlite')
        cursor = conn.cursor()

        cursor.executescript(create_query)
        conn.commit()

        cursor.executemany('INSERT INTO tests(name, command) VALUES (?,?)', tests)
        conn.commit()

        cursor.close()
        conn.close()
