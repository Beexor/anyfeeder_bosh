import functools
import operator
import sqlite3

class Test:
    def __init__(self, name,id = None, command=None):
        self.name= name
        if command is None:
            self.command = self.get_command()
        else:
            self.command = command

        if id is None:
            self.id = self.get_id()
        else:
            self.id = id

    def __str__(self):
        return "id: {0} name: {1} command: {2}".format(self.id, self.name, self.command)

    def execute(self):
        #TODO serial connection
        print("WIP running tests")

        print("WIP tests done")
        return "Response: {0}, {1}".format(self.name, self.command)

    def get_command(self):
        sql = 'SELECT command FROM tests WHERE name=?'
        output = self.select_test(sql, (self.name,))
        return output

    def get_id(self):
        sql = 'SELECT ID FROM tests WHERE name=?'
        output = self.select_test(sql, (self.name,))
        return functools.reduce(operator.add, output)

    def select_test(self, sql, name):
        conn = sqlite3.connect('db/AnyfeederBosch.sqlite')
        cursor = conn.cursor()
        print(sql)
        cursor.execute(sql, name)
        output = cursor.fetchone()
        print(output)
        cursor.close()
        conn.close()
        return output

